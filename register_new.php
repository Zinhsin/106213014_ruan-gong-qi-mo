<?php
require_once("dbconfig.php");;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login UI</title>
<script type="text/javascript">
window.onload = init;
function init() {
    document.getElementById("f1").onsubmit = check;
}
function check() {
    var username=document.f1.ID.value;
    var regex=/^[/s]+$/;//聲明一個判斷用户名前後是否有空格的正則表達式
    p1 = document.f1.PWD.value;
    p2 = document.f1.PWD2.value;
    if (username == "") {
        alert("帳號不可空白，請重新輸入!");
        document.f1.ID.focus();
        return false;
    }
    if (p1 =="" || p2 =="") {
        alert("密碼不可空白，請重新輸入!");
        document.f1.PWD.focus();
        return false;
    }
    if (p1 == p2)
        return true;
    else {
        document.f1.PWD.value = "";
        document.f1.PWD2.value="";
        alert("密碼不一致，請重新輸入!");
        document.f1.PWD.focus();
        return false;
    }
}
</script>
</head>
<body>
<p>Register UI</p>
<hr>
<!-- form -->
<p>Enter ID and Password to register</p>
<form id ="f1" name ="f1" method="post" action="register_new.php" >
ID: <input type="text" name="ID" > <br>
Password: <input type="password" name="PWD" > <br>
Check password:<input type="password" name="PWD2"> <br>
<input type="submit"> <input type="reset" value="reset">

</form>
<hr>
</body>
</html>
