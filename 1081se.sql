-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- 主機: 127.0.0.1
-- 產生時間： 
-- 伺服器版本: 10.1.35-MariaDB
-- PHP 版本： 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `1081se`
--

-- --------------------------------------------------------

--
-- 資料表結構 `game`
--

CREATE TABLE `game` (
  `id` int(11) NOT NULL,
  `expire` datetime NOT NULL,
  `name` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 資料表的匯出資料 `game`
--

INSERT INTO `game` (`id`, `expire`, `name`) VALUES
(1, '2019-12-25 12:02:29', 'No 1'),
(2, '2016-11-28 00:15:00', 'No 2');

-- --------------------------------------------------------

--
-- 資料表結構 `orderitem`
--

CREATE TABLE `orderitem` (
  `serno` int(11) NOT NULL,
  `ordID` int(11) NOT NULL,
  `prdID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 資料表的匯出資料 `orderitem`
--

INSERT INTO `orderitem` (`serno`, `ordID`, `prdID`, `quantity`) VALUES
(3, 4, 1, 1),
(4, 4, 2, 1),
(44, 6, 1, 5),
(45, 6, 3, 1),
(46, 8, 4, 1),
(47, 9, 1, 2),
(48, 9, 2, 2),
(49, 9, 3, 1),
(50, 9, 4, 1),
(51, 10, 1, 2),
(52, 10, 4, 1),
(53, 10, 3, 1),
(54, 8, 1, 1),
(55, 8, 5, 1),
(56, 11, 1, 2),
(57, 11, 3, 1),
(58, 11, 7, 2),
(59, 12, 2, 1),
(60, 12, 3, 1),
(61, 12, 7, 1),
(62, 13, 7, 1),
(64, 15, 2, 1),
(65, 15, 3, 1),
(66, 16, 2, 1),
(67, 16, 3, 1),
(68, 16, 7, 1),
(69, 17, 2, 1),
(70, 17, 3, 1),
(71, 17, 7, 1),
(72, 18, 2, 1),
(73, 19, 3, 1),
(74, 20, 7, 1),
(75, 21, 3, 1),
(76, 22, 7, 1),
(77, 23, 3, 1),
(78, 23, 7, 1),
(79, 24, 3, 1),
(80, 25, 3, 1),
(81, 26, 3, 1),
(82, 26, 7, 1),
(83, 27, 3, 1),
(84, 27, 7, 1),
(85, 28, 2, 1),
(86, 29, 2, 1),
(87, 30, 2, 1),
(88, 13, 2, 1),
(89, 13, 3, 1),
(90, 32, 2, 1),
(91, 32, 3, 1),
(92, 33, 2, 1);

-- --------------------------------------------------------

--
-- 資料表結構 `product`
--

CREATE TABLE `product` (
  `prdID` int(11) NOT NULL,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `detail` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 資料表的匯出資料 `product`
--

INSERT INTO `product` (`prdID`, `name`, `price`, `detail`) VALUES
(2, 'Water', 90, 'Pure water from Puli'),
(3, 'Air', 0, 'PM 2.5 air for free'),
(7, 'iphone', 5000, 'this is a expensive phone');

-- --------------------------------------------------------

--
-- 資料表結構 `user`
--

CREATE TABLE `user` (
  `ID` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) NOT NULL DEFAULT '0',
  `badpoints` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 資料表的匯出資料 `user`
--

INSERT INTO `user` (`ID`, `password`, `name`, `role`, `badpoints`) VALUES
('admin', '123', '管理員', 9, 1),
('hehe', '123', '物流人員', 15, 1),
('Omama', '321', '客戶', 1, 1),
('rin', '123', '客戶', 1, 1),
('test', '123', '客戶', 1, 3),
('test2', '123', '客戶', 1, 12),
('user', '123', '客戶', 1, 6);

-- --------------------------------------------------------

--
-- 資料表結構 `userorder`
--

CREATE TABLE `userorder` (
  `ordID` int(11) NOT NULL,
  `uID` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `orderDate` date NOT NULL,
  `address` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 資料表的匯出資料 `userorder`
--

INSERT INTO `userorder` (`ordID`, `uID`, `orderDate`, `address`, `status`) VALUES
(4, 'admin', '0000-00-00', '', 1),
(5, 'admin', '0000-00-00', '', 0),
(6, 'user', '2019-12-30', 'ncnu im', 3),
(9, 'Omama', '2019-12-30', 'ncnu im', 2),
(10, 'rin', '2019-12-30', 'ncnu im', 2),
(14, 'test2', '0000-00-00', '', 0),
(31, 'test1', '0000-00-00', 'AAA', 3),
(32, 'test', '2020-01-08', 'ncnu im', 3);

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `orderitem`
--
ALTER TABLE `orderitem`
  ADD PRIMARY KEY (`serno`);

--
-- 資料表索引 `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`prdID`);

--
-- 資料表索引 `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`);

--
-- 資料表索引 `userorder`
--
ALTER TABLE `userorder`
  ADD PRIMARY KEY (`ordID`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `game`
--
ALTER TABLE `game`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用資料表 AUTO_INCREMENT `orderitem`
--
ALTER TABLE `orderitem`
  MODIFY `serno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- 使用資料表 AUTO_INCREMENT `product`
--
ALTER TABLE `product`
  MODIFY `prdID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- 使用資料表 AUTO_INCREMENT `userorder`
--
ALTER TABLE `userorder`
  MODIFY `ordID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
