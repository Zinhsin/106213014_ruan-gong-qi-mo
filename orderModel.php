<?php
require_once("dbconfig.php");

function getOrderList($uID) {
    global $db;
    $sql = "SELECT ordID, orderDate, status FROM userOrder WHERE uID=?";
    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
    mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
    mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
    return $result;
}

function getConfirmedOrderList() {
    global $db;
    $sql = "SELECT ordID, uID, orderDate FROM userOrder WHERE status=1";
    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
    //mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
    mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
    return $result;
}
function getShippingOrderList() {
    global $db;
    $sql = "SELECT * FROM userOrder WHERE status=2 order by address";
    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
    //mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
    mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
    return $result;
}
function getBestSoldOrderList() {
    global $db;
    $sql = "SELECT orderitem.prdID,SUM(orderitem.quantity) total FROM `orderitem` ,`product` WHERE orderitem.prdID = product.prdID GROUP by orderitem.prdID order by SUM(orderitem.quantity) DESC";
    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
    //mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
    mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
    return $result;
}
function getVIP() {
    global $db;
    $sql = "SELECT userorder.uID, userorder.ordID, orderitem.ordID,SUM(product.price * orderitem.quantity) total FROM `orderitem`,`userorder`,`product` WHERE orderitem.ordID = userorder.ordID AND product.prdID = orderitem.prdID GROUP BY userorder.uID ORDER by total DESC";
    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
    //mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
    mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
    return $result;
}
function _getCartID($uID) {
    //get an unfished order (status=0) from userOrder
    global $db;
    $sql = "SELECT ordID FROM userorder WHERE uID=? and status=0";
    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
    mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
    mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
    if ($row=mysqli_fetch_assoc($result)) {
        return $row["ordID"];
    } else {
        //no order with status=0 is found, which means we need to creat an empty order as the new shopping cart
        $sql = "insert into userOrder ( uID, status ) values (?,0)";
        $stmt = mysqli_prepare($db, $sql); //prepare sql statement
        mysqli_stmt_bind_param($stmt, "s", $uID); //bind parameters with variables
        mysqli_stmt_execute($stmt);  //執行SQL
        $newOrderID=mysqli_insert_id($db);
        return $newOrderID;
    }
}

function addToCart($uID, $prdID) {
	global $db;
    $ordID= _getCartID($uID);
    $prd = array();
    $sql_first="SELECT * FROM `orderItem` WHERE `ordID` = ?;";
    $stmt_first = mysqli_prepare($db, $sql_first);
    mysqli_stmt_bind_param($stmt_first, "i", $ordID);
    mysqli_stmt_execute($stmt_first);
    $result = mysqli_stmt_get_result($stmt_first);
    while ($row=mysqli_fetch_array($result)) {
		array_push($prd ,$row["prdID"]);
		//print($prd);
		//echo '<script>console.log('.$prd.')</script>';
	}
    if(in_array($prdID,$prd)){
        $sql = "update `orderItem` set `quantity` = `quantity` +1 where prdID = ? and ordID = ?;";
        $stmt = mysqli_prepare($db, $sql); //prepare sql statement
        mysqli_stmt_bind_param($stmt, "ii", $prdID, $ordID); //bind parameters with variables
        return mysqli_stmt_execute($stmt);  //執行SQL
    }else{
        $sql = "insert into orderItem (ordID, prdID, quantity) values (?,?,1);";
        $stmt = mysqli_prepare($db, $sql); //prepare sql statement
        mysqli_stmt_bind_param($stmt, "ii", $ordID, $prdID); //bind parameters with variables
        return mysqli_stmt_execute($stmt);  //執行SQL
    }

}

function removeFromCart($serno) {
    global $db;
    $sql = "delete from orderItem where serno=?;";
    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
    mysqli_stmt_bind_param($stmt, "i", $serno); //bind parameters with variables
    return mysqli_stmt_execute($stmt);  //執行SQL
}
function removeOrder($ordID) {
    global $db;
    $sql2 = "SELECT * from `userorder` where ordID = ?;";
    $stmt2 = mysqli_prepare($db, $sql2); //prepare sql statement
    mysqli_stmt_bind_param($stmt2, "i", $ordID);
    mysqli_stmt_execute($stmt2);
    $result = mysqli_stmt_get_result($stmt2);
    while ($row=mysqli_fetch_array($result)){
        $uID = $row["uID"];
    }
    

    $sql = "DELETE from userorder where ordID=? ;";
    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
    mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
    mysqli_stmt_execute($stmt);
    // 更改
    //$uID = 'test2';
    

	
    $sql3 = "update `user` set `badpoints` = `badpoints` +1 WHERE ID =?;";
    $stmt3 = mysqli_prepare($db, $sql3); //prepare sql statement
    mysqli_stmt_bind_param($stmt3, "s", $uID); //bind parameters with variables
    
    return mysqli_stmt_execute($stmt3);  //執行SQL
}
function checkout($uID, $address) {
    global $db;
    $ordID= _getCartID($uID);
    $sql = "update userorder set orderDate=now(),address=?,status=1 where ordID=?;";
    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
    mysqli_stmt_bind_param($stmt, "si", $address, $ordID); //bind parameters with variables
    return mysqli_stmt_execute($stmt);  //執行SQL
}

function shipout($ordID) {
    global $db;
    $sql = "update userorder set status=2 where ordID=?;";
    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
    mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
    return mysqli_stmt_execute($stmt);  //執行SQL
}
function arrive($ordID) {
    global $db;
    $sql = "update userorder set status=3 where ordID=?;";
    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
    mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
    return mysqli_stmt_execute($stmt);  //執行SQL
}

function getCartDetail($uID) {
    global $db;
    $ordID= _getCartID($uID);
    $sql="select orderItem.serno, product.name, product.price, orderItem.quantity, product.detail from orderItem, product where orderItem.prdID=product.prdID and orderItem.ordID=?";
    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
    mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
    mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
    return $result;
}


function getOrderDetail($ordID) {
    global $db;
    $sql="select orderItem.serno, product.name, product.price, orderItem.quantity from orderItem, product where orderItem.prdID=product.prdID and orderItem.ordID=?";
    $stmt = mysqli_prepare($db, $sql); //prepare sql statement
    mysqli_stmt_bind_param($stmt, "i", $ordID); //bind parameters with variables
    mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
    return $result;
}
?>










